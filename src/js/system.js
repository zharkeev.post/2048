import Hammer from 'hammerjs'
import { Square } from './square'
import { getRandNum } from './helpers'
import { state } from './state'

function startMove(dir) {
  new Square(getRandNum(), dir)
}

export function initGame() {
  for (let i = 0; i < state.getInitSquaresQuantity(); i += 1) {
    new Square(getRandNum())
  }
}

export function initNewGame() {
  state.resetScore()
  state.getUpdatedDomSquares().forEach((square) => {
    square.remove()
  })
  initGame()
}

export function loadGameFromLs(lsData) {
  lsData.squares.forEach((square) => new Square(square.value, null, square.position))
  state.setScore(lsData.score)
  state.setSoundStatus(lsData.soundStatus)
}

export function initMove(e) {
  const posKey = ['ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight']
  posKey.forEach((key) => {
    if (e.key === key) startMove(key)
  })
}

export function listenSwipe() {
  const swipeArea = state.getRefs().table
  const hammerTime = new Hammer(swipeArea)
  hammerTime.get('swipe').set({
    direction: Hammer.DIRECTION_ALL,
    pointers: 1
  })
  hammerTime.on('swipe', (event) => {
    const dirNum = event.direction
    switch (dirNum) {
      case 8:
        startMove('ArrowUp')
        break
      case 16:
        startMove('ArrowDown')
        break
      case 2:
        startMove('ArrowLeft')
        break
      case 4:
        startMove('ArrowRight')
        break
      default:
        break
    }
  })
}
